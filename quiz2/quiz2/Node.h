#pragma once
#ifndef NODE_H
#define NODE_H

#include <string>
using namespace std;
struct Node
{
	string name;
	Node* next = NULL;
	Node* previous = NULL;
};

#endif //NODE_H
