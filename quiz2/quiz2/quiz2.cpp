#include <iostream>
#include <time.h>
#include "Node.h"
using namespace std;

void addPerson(Node** start, string value);

void display(struct Node* start);

void eliminatePerson(Node** start, string key);

int getRandPosition(int min, int max);

string selectPerson(Node* start, int no);

int main() {
	Node* head = NULL;
	int noOfPersons;
	cout << "How many Soldiers are availabe to participate: ";
	cin >> noOfPersons;
	string name;
	for (int i = 0; i < noOfPersons; i++) {
		cout << "What's your name solider? ";
		cin >> name;
		addPerson(&head, name);
	}
	int rp = getRandPosition(1, noOfPersons);
	cout << "Select " << rp << endl;
	string selected = selectPerson(head, rp);
	int round = 1;
	while (noOfPersons > 1) {
		cout << "============================================\n";
		cout << "ROUND " << round << '\n';
		cout << "============================================\n";
		cout << "Remaining Members:\n";
		display(head);
		cout << "\n\nResult:\n";
		rp = getRandPosition(1, noOfPersons);
		cout << "  " << selected << " has drawn " << rp << '\n';
		Node* temp = head;
		while (temp->name != selected)
			temp = temp->next;
		int i = 0;
		while (i < rp) {
			temp = temp->next;
			i++;
		}
		string victom = temp->name;
		selected = temp->next->name;
		eliminatePerson(&head, victom);
		cout << "  " << victom << " was eliminated\n";
		noOfPersons--;
		round++;
	}
	cout << "============================================\n";
	cout << "FINAL RESULT\n";
	cout << "============================================\n";
	cout << "  " << selectPerson(head, 0) << " will go to seek for reinforcements.\n";
	system("pause");
	return 0;
}


string selectPerson(Node* start, int no) {
	if (!start) 
		return "";
	Node* temp = start;
	int i = 1;
	while (temp->next != start && i < no)
	{
		temp = temp->next;
		i++;
	}
	return temp->name;
}

int getRandPosition(int min, int max) {
	srand(time(0));
	int rd = min + rand() % max;
	return rd;
}

void addPerson(Node** start, string value)
{
	if (*start == NULL) 
	{
		Node* new_node = new Node;
		new_node->name = value;
		new_node->next = new_node->previous = new_node;
		*start = new_node;
		return;
	}
	Node* last = (*start)->previous;
	struct Node* new_node = new Node;
	new_node->name = value;
	new_node->next = *start;
	(*start)->previous = new_node;
	new_node->previous = last;
	last->next = new_node;
}

void display(struct Node* start) 
{
	Node* temp = start;
	while (temp->next != start)
	{
		cout << "  " << temp->name << "\n";
		temp = temp->next;
	}
	cout << "  " << temp->name << '\n';

}

void eliminatePerson(Node** start, string key)
{
	if (*start == NULL)
		return;
	struct Node* curr = *start, *prev_1 = NULL;
	while (curr->name != key) { 
		if (curr->next == *start) {
			return;
		}
		prev_1 = curr;
		curr = curr->next;
	}
	if (curr->next == *start && prev_1 == NULL) {
		(*start) = NULL;
		free(curr);
		return;
	}
	if (curr == *start) {
		prev_1 = (*start)->previous;
		*start = (*start)->next;
		prev_1->next = *start;
		(*start)->previous = prev_1;
		delete curr;
	}

	
	else if (curr->next == *start) {
		prev_1->next = *start;
		(*start)->previous = prev_1;
		delete curr;
	}
	else {
		struct Node* temp = curr->next;

		prev_1->next = temp;
		temp->previous = prev_1;
		delete curr;
	}
}