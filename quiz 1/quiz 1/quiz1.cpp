#include "pch.h"
#include <iostream> 
#include <stdlib.h>
#include <string>
#include <list>
#include <vector>

using namespace std;

struct item
{
	string name;
	int gold;
};

struct NPC
{
	string npcName;
	item itemOrder;
};

string NPCGold[4][2];
string GuildStore[10][3];
string NPCItem[10000][2];

NPC newnpc;
string pNPCNameCurrent;
string pGSItem;
int pNPCCurrentGold;
int pGetGoldBuying;

void  PopulateNPCGold() {

	NPCGold[0][0] = "Man";
	NPCGold[1][0] = "Old Man";
	NPCGold[2][0] = "Woman";
	NPCGold[3][0] = "Girl";

	NPCGold[0][1] = "1000";
	NPCGold[1][1] = "1000";
	NPCGold[2][1] = "1000";
	NPCGold[3][1] = "1000";
}

void  PopulateGuildStore() {
	
	GuildStore[0][0] = "Wooden Armband";
	GuildStore[1][0] = "Leather Glove";
	GuildStore[2][0] = "Wool Hat";
	GuildStore[3][0] = "Focus Staff";
	GuildStore[4][0] = "Wooden Shield";
	GuildStore[5][0] = "Longsword";
	GuildStore[6][0] = "Crafter's Knife";
	GuildStore[7][0] = "Longbow";
	GuildStore[8][0] = "Willpower Ring";
	GuildStore[9][0] = "Knight's Helm";

	GuildStore[0][1] = "15";
	GuildStore[1][1] = "20";
	GuildStore[2][1] = "25";
	GuildStore[3][1] = "30";
	GuildStore[4][1] = "35";
	GuildStore[5][1] = "120";
	GuildStore[6][1] = "140";
	GuildStore[7][1] = "140";
	GuildStore[8][1] = "600";
	GuildStore[9][1] = "900";

	GuildStore[0][2] = "23";
	GuildStore[1][2] = "33";
	GuildStore[2][2] = "28";
	GuildStore[3][2] = "55";
	GuildStore[4][2] = "62";
	GuildStore[5][2] = "122";
	GuildStore[6][2] = "272";
	GuildStore[7][2] = "186";
	GuildStore[8][2] = "1083";
	GuildStore[9][2] = "1596";

}

void DefineNPCItem() { 
	NPCItem[0][0] = "";
	NPCItem[1][0] = "";
}

void DisplayGuildShop()
{
	std::cout << "Guild Items:" << endl;
	cout << "==============" << endl;
	cout << "1) Wooden Armband | Gold: 15" << endl;
	cout << "2) Leather Glove | Gold: 20" << endl;
	cout << "3) Wool Hat | Gold: 25" << endl;
	cout << "4) Focus Staff | Gold: 30" << endl;
	cout << "5) Wooden Shield | Gold: 35" << endl;
	cout << "6) Longsword | Gold: 120" << endl;
	cout << "7) Crafter's Knife | Gold: 140" << endl;
	cout << "8) Longbow |  Gold: 140" << endl;
	cout << "9) Willpower Ring | Gold: 600" << endl;
	cout << "10) Knight's Helm | Gold: 900" << endl << endl;
	cout << "Enter item to buy (0 to Quit):";
}

void DisplayNPCInformation() {

	cout << "Current gold: " << pNPCCurrentGold << endl;

}

void InsertInsertNPCItem(string NPCName, string Item) {
	int x;

	for (x = 0; x <= 10000; x++)
	{
		if (NPCItem[x][0] == "") {
			NPCItem[x][0] = NPCName;
			NPCItem[x][1] = Item;
			break;
		}
	}
}

void DisplayNPCInventory(string NPCName) {
	int x;
	cout << "Invetory items:" << endl;
	for (x = 0; x <= 10000; x++)
	{
		if (NPCItem[x][0] == NPCName) {
			cout << NPCItem[x][1] << endl; 
		}
		if (NPCItem[x][0] == "") {
			cout << endl;
			break;
		}
	}
}

string generateRandomNPCName() {

	string arr[4] = { "Man", "Old Man", "Woman", "Girl" };

	int i = rand() % 4;
	newnpc.npcName = arr[i];

	pNPCNameCurrent = newnpc.npcName;

	return pNPCNameCurrent;
}

string generateRandomNPCNameForSelling() {

	string arr[4] = { "Man", "Old Man", "Woman", "Girl" };

	int i = rand() % 4;
	string npcnameforsell;
	npcnameforsell = arr[i];

	return npcnameforsell;
}

string generateRandomItem() {

	string arr[10] = { "Wooden Armband", "Leather Glove", "Wool Hat", "Focus Staff", "Wooden Shield", 
					   "Longsword", "Crafter's Knife", "Longbow", "Willpower Ring", "Knight's Helm" };

	int i = rand() % 9;
	newnpc.itemOrder.name = arr[i];

	pGSItem = newnpc.itemOrder.name;

	return pGSItem;
}

int GBGetCurrentGold(string NPCName) {
	int res = 0;
	int x;

	for (x = 0; x <= 3; x++)
	{
		if (NPCGold[x][0] == NPCName) {
			res = std::stoi(NPCGold[x][1].c_str());
			break;
		}
	}
	return res;
}

int GBGetGoldBuying(string Item) {
	int res = 0;
	int x;

	for (x = 0; x <= 9; x++)
	{
		if (GuildStore[x][0] == Item) {
			res = stoi(GuildStore[x][1].c_str());
			break;
		}
	}
	return res;
}

int GetGoldSelling(string Item) {
	int res = 0;
	int x;

	for (x = 0; x <= 9; x++)
	{
		if (GuildStore[x][0] == Item) {
			res = stoi(GuildStore[x][2].c_str());
			break;
		}
	}
	return res;
}

void UpdateCurrentGold(string NPCName, string CurrentGold) {

	int res = 0;
	int x;

	for (x = 0; x <= 3; )
	{
		if (NPCGold[x][0] == NPCName) {
			string nm;
			nm = NPCGold[x][0];

			NPCGold[x][1] = CurrentGold;
			string gd;
			gd = NPCGold[x][1];

			break;
		}
		x = x + 1;
	}

}

bool NPCItemAvailable(string NPCName, string Item) {
	int x;

	for (x = 0; x <= 10000; x++)
	{
		if (NPCItem[x][0] == "") { 
			return false;
			break;
		}
		if (NPCItem[x][0] == NPCName && NPCItem[x][1] == Item) {
			return true;
			break;
		}
	}

	return false;
}


void DeleteNPCName(string NPCName, string Item) {
	int x;

	for (x = 0; x <= 10000; x++)
	{
		if (NPCItem[x][0] == "") { 
			break;
		}
		if (NPCItem[x][0] == NPCName && NPCItem[x][1] == Item) {
			NPCItem[x][0] = "Record Deleted";
			NPCItem[x][1] = "Record Deleted";
			break;
		}
	}

}

int main()
{
	bool fromNPCShop;
	fromNPCShop = false;

	PopulateNPCGold();
	PopulateGuildStore();
	DefineNPCItem();

	generateRandomNPCName();
	
	newnpc.itemOrder.gold = 1000; 
	cout << "Current gold: " << newnpc.itemOrder.gold << endl;

	cout << "Inventory items:" << endl;
	cout << "EMPTY!!" << endl << endl;

gotoStore:
	
	if (fromNPCShop == true) {

		system("cls");

		DisplayNPCInformation();

		DisplayNPCInventory(pNPCNameCurrent);

	}

	DisplayGuildShop();

	int option;
	string goOn;

	do
	{
		cin >> option;
		double result = 0.0;
		string item;

		switch (option)
		{
		case 1:
			item = "Wooden Armband";
			result = option;
			break;
		case 2:
			item = "Leather Glove";
			result = option;
			break;
		case 3:
			item = "Wool Hat";
			result = option;
			break;
		case 4:
			item = "Focus Staff";
			result = option;
			break;
		case 5:
			item = "Wooden Shield";
			result = option;
			break;
		case 6:
			item = "Longsword";
			result = option;
			break;
		case 7:
			item = "Crafter's Knife";
			result = option;
			break;
		case 8:
			item = "Longbow";
			result = option;
			break;
		case 9:
			item = "Willpower Ring";
			result = option;
			break;
		case 10:
			item = "Knight's Helm";
			result = option;
			break;
		}

		if ((option >= 1) && (option <= 10)) { 
			
			pNPCCurrentGold = GBGetCurrentGold(pNPCNameCurrent);
			pGetGoldBuying = GBGetGoldBuying(item);

			if (pNPCCurrentGold >= pGetGoldBuying) {
				pNPCCurrentGold = pNPCCurrentGold - pGetGoldBuying;
				newnpc.itemOrder.gold = pNPCCurrentGold;

				string gold = to_string(pNPCCurrentGold);
				UpdateCurrentGold(pNPCNameCurrent, gold); 

				
				system("cls");


				DisplayNPCInformation();

				InsertInsertNPCItem(pNPCNameCurrent, item);
				DisplayNPCInventory(pNPCNameCurrent);

				DisplayGuildShop();

				cout << item << " bought!" << endl;
			}
			else {
				
				system("cls");


				DisplayNPCInformation();

				DisplayNPCInventory(pNPCNameCurrent);

				DisplayGuildShop();

				cout << "You don't have enough money to buy that item!" << endl;

			}

		}
		else {
			
			if (option == 0) {

				system("cls");
				
				cout << "Begin shop!" << endl;

				DisplayNPCInformation();
				
				string GSNPCName;
				string GSItem;

				int tranNo = 1;
				do {
					GSNPCName = generateRandomNPCNameForSelling();
					GSItem = generateRandomItem();

					cout << GSNPCName << " is looking for " << GSItem << endl;
					bool avail;

					avail = NPCItemAvailable(pNPCNameCurrent, GSItem);
					if (avail == true) {

							
							int gsgoldsell;
							gsgoldsell = GetGoldSelling(GSItem);
							pNPCCurrentGold = pNPCCurrentGold + gsgoldsell;

							
							UpdateCurrentGold(pNPCNameCurrent, to_string(pNPCCurrentGold));  

							
							if (pNPCCurrentGold == 10000) {
								cout << "Winner!" << endl;
							}
							
							
							DeleteNPCName(pNPCNameCurrent, GSItem);

							
							InsertInsertNPCItem(GSNPCName, GSItem);

							int sellingcurgold;
							sellingcurgold = GBGetCurrentGold(GSNPCName);
							sellingcurgold = sellingcurgold - gsgoldsell;
							UpdateCurrentGold(GSNPCName, to_string(sellingcurgold));  

							cout << "Sold for " << gsgoldsell << " gold!" << endl;

					}
					else {
						cout << "The item isn't in your shop, customer left...." << endl;
					}
					

					tranNo = tranNo + 1;
				} while (tranNo <= 5);

				cout << "Press 0 to return to store..." << endl;
				int a;
				cin >> a;

				if (a == 0) {
					
					fromNPCShop = true;
					goto gotoStore;
				}

			}
			else {
				cout << "Invalid option" << endl;
			}
		}

	} while ((option >= 1) && (option <= 10));

}