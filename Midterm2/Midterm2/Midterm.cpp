#include <iostream>
#include<string>
#include<vector>
#include<time.h>
#include<iomanip>
#include<windows.system.h>
using namespace std;


void play();//playing each turn
void initializeOptions(); //initializes the vector for the cards choices
string generateRandomCard(bool); //returns a random card
int evaluateCards(string, string); //returns the result of two cards
void Game(); //main game function
void initializeTurns(); //array that contains turns order

int cash = 0; //initial cash
int dist = 30; //distance from eardrum
int roundNum = 0; //round number for each turn
bool turn[12]; // 1 for emperor and 0 for slave
int num;
bool gameFlag = true;
string side = "";
int cardNum;
int optionCount = 5;
vector<string> option;

int main(int argc, char** argv) {
	initializeTurns();
	Game();
	system("pause");


	return 0;
}
string generateRandomCard(bool flag) {

	if (flag == true) {
		string choices[5] = { "Slave","Citizen","Citizen" ,"Citizen" ,"Citizen" };
		int t = rand() % 5;
		return choices[t];
	}
	else {

		string choices[5] = { "Emperor","Citizen","Citizen","Citizen","Citizen" };
		int t = rand() % 5;
		return choices[t];
	}
	return "";
}

void play() {
	bool flag = true;
	initializeOptions();
	option[0] = side;

	while (flag) {
		system("cls");
		srand(time(0));
		cout << "Pick a card to play...\n";
		cout << "======================\n";

		for (int i = 0; i < option.size(); i++) {
			cout << setw(8) << "[" << i + 1 << "] " << option[i] << "\n";
		}

		cin >> cardNum;
		if (cardNum > option.size()) {
			cout << "Invalid choice entered!!\n";
			system("pause");
			exit(0);
		}

		string randomCard = generateRandomCard(turn[roundNum]);
		int status = evaluateCards(option[cardNum - 1], randomCard);
		system("cls");
		cout << "Open!\n\n";
		cout << "[Kaiji] " << option[cardNum - 1] << " vs [Tonegawa] " << randomCard << "\n\n";

		if (status == 2) {
			cout << "Draw!\n";
			option.pop_back();
		}
		else if (status == 1) {
			int prize;
			if (turn[roundNum] == 1) {
				prize = 100000;
			}
			else {
				prize = 500000;
			}
			int amount = num * prize;
			cout << "You won " << amount << " yen.\n\n";
			cash = cash + amount;
			roundNum++;
			flag = false;

		}
		else if (status == -1) {

			cout << "You lost! The pin will now move by " << num << "mm\n";
			cout << "Drill....drill....drill...drill...\n";
			cout << "Kaiji: Gaaaaaaaaaaaaaaaaaaaaaaaahhhhhh...\n\n";
			dist = dist - num;
			if (dist <= 0) {
				system("pause");
				system("cls");
				cout << "The pin has pierced your eardrum.\n";
				cout << "No matter how much money you've won, it will not bring back what you've lost.\n";
				cout << "Tonegawa will not be kneeling down on a loser like you.\n\n";
				system("pause");
				exit(0);
			}
			roundNum++;
			flag = false;

		}
		system("pause");

	}
}


int evaluateCards(string card1, string card2) {

	if (card1 == "Emperor") {
		if (card2 == "Citizen") {
			return 1;
		}
		else if (card2 == "Slave")
		{
			return -1;
		}
		else {
			cout << "Error in line checks!!";
			return 0;
		}
	}
	else if (card1 == "Slave") {
		if (card2 == "Citizen") {
			return -1;
		}
		else if (card2 == "Emperor")
		{
			return 1;
		}
		else {
			cout << "Error in checks!";
			return 0;
		}
	}
	else if (card1 == "Citizen") {
		if (card2 == "Citizen") {
			return 2;
		}
		else if (card2 == "Slave") {
			return 1;
		}
		else if (card2 == "Emperor") {
			return -1;
		}
		else {
			cout << "Error in checks!";
			return 0;
		}
	}
	return 0;
}

void Game() {


	while (gameFlag == true) {

		system("cls");
		cout << "Cash: " << cash << "\n";
		cout << "Distance left (mm): " << dist << "\n";
		cout << "Round: " << roundNum + 1 << "/12\n";
		if (turn[roundNum] == 1) {
			side = "Emperor";
		}
		else {
			side = "Slave";
		}

		cout << "Side: " << side << "\n\n";
		cout << "How many mm would you like to wager, Kaiji? ";
		cin >> num;
		while (num > dist || num < 0) {
			cout << "How many mm would you like to wager, Kaiji? ";
			cin >> num;
		}

		play();
		if (cash == 20000000) {
			system("cls");
			cout << "Congratulations kaiji!!You managed to won 20000000 yen.";
			cout << "This time, your luck went along with you.\n\n";
			system("pause");
			exit(0);
		}
		else if (roundNum == 12) {

			system("cls");
			cout << "You did not reach 20000000 yen.\n";
			cout << "Tonegwa laughs as you dig yourself into despair.\n";
			cout << "You will be forever haunted by the souls of those who died.\n\n";
			system("pause");

			exit(0);

		}


	}
}



void initializeTurns() {
	turn[0] = 1;
	turn[1] = 1;
	turn[2] = 1;
	turn[3] = 0;
	turn[4] = 0;
	turn[5] = 0;
	turn[6] = 1;
	turn[7] = 1;
	turn[8] = 1;
	turn[9] = 0;
	turn[10] = 0;
	turn[11] = 0;
}

void initializeOptions() {

	while (!option.empty()) {
		option.pop_back();
	}

	option.push_back("");
	for (int i = 0; i < 4; i++) {
		option.push_back("Citizen");
	}
}